import  { Person }  from "./person.js";
class Student extends Person{
    __standard;
    __collegeName;
    __grade;

    constructor(paramStandard,paramCollegeName,paramGrade)
    {
        super(paramStandard,paramCollegeName,paramGrade);
        this._personName;
        this._personAge;
        this._gender;
        this.__standard=paramStandard;
        this.__collegeName=paramCollegeName;
        this.__grade=paramGrade;
    }

    getStudentInfo()
    {
        return this.__grade +" - " +this.__collegeName+" - "+this.__standard;
    }

    getGrade()
    {
        return this.__grade;
    }

    setGrade(paramGrade)
    {
        this.__grade=paramGrade;
    }
}

export {Student}