import  { Person }  from "./person.js";
import { Student } from "./student.js";
var person1=new Person("Jonh",30,"Nam");

console.log(person1);
console.log(person1 instanceof Person);
console.log(person1 instanceof Student);

var student1= new Student("silver","Node",2);

console.log(student1);
console.log(student1.getStudentInfo());
student1.setGrade(3);
console.log(student1.getGrade());
console.log(student1 instanceof Student);
console.log(student1 instanceof Person);
